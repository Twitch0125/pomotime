/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    fontFamily: {
      body: ['Inter'],
      display: ['"Work Sans"'],
      mono: ['"Roboto Mono"', 'monospace']
    }
  },
  variants: {},
  plugins: []
}
