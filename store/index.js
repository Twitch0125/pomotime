import { DateTime, Interval } from 'luxon'
const defaultState = {
  workPeriod: 25,
  workIterations: 4,
  shortBreak: 5,
  longBreak: 15,
  time: {},
  endTime: {},
  timeInitialized: false
}
export const state = () => ({
  workPeriod: 25,
  workIterations: 4,
  shortBreak: 5,
  longBreak: 15,
  time: {},
  endTime: {},
  timeInitialized: false
})

export const mutations = {
  SET_TIME(state, data) {
    state.time = data
  },
  SET_END_TIME(state, data) {
    state.endTime = data
  },
  SET_TIME_INITIALIZED(state, data) {
    state.timeInitialized = data
  },
  SET_STATE(state, data) {
    state = { ...state, data }
  }
}

export const actions = {
  initTime({
    commit,
    getters: { timeRemaining },
    state: { workPeriod, timeInitialized, time, endTime }
  }) {
    if (!timeInitialized) {
      const newEndTime = DateTime.local().plus({ minutes: workPeriod })
      const currentTime = DateTime.local()
      commit('SET_END_TIME', newEndTime)
      commit('SET_TIME', currentTime)
      commit('SET_TIME_INITIALIZED', true)
    }
  },
  setTime({ commit }, payload) {
    commit('SET_TIME', payload)
  },
  setEndTime({ commit }, payload) {
    commit('SET_END_TIME', payload)
  },
  reset({ commit }) {
    commit('SET_STATE', defaultState)
  }
}

export const getters = {
  time({ time }) {
    return time
  },
  workPeriod({ workPeriod }) {
    return workPeriod
  },
  workIterations({ workIterations }) {
    return workIterations
  },
  shortBreak({ shortBreak }) {
    return shortBreak
  },
  longBreak({ longBreak }) {
    return longBreak
  },
  timeRemaining({ time, endTime }) {
    return DateTime.fromMillis(
      DateTime.fromISO(endTime).valueOf() - DateTime.fromISO(time).valueOf()
    )
    // return Interval.fromDateTimes(time, endTime)
  }
}
